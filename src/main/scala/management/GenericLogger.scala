// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package management

import management.alerts.Mailgun
import management.jobutils.JobAPI

import com.databricks.dbutils_v1.DBUtilsHolder.dbutils
import io.delta.tables._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import java.sql.Timestamp

class GenericLogger(spark: SparkSession)  extends CrawlLogging {
  import spark.implicits._
  private val schema = "warehouse"
  private val log_table = "crawl_logging"
  private val events_table = "crawl_events"
  private val token_table = "token_logging"

  override def init(): Unit = {
    spark.sql(s"DROP TABLE IF EXISTS $schema.$log_table")
    spark.sql(s"DROP TABLE IF EXISTS $schema.$events_table")
    spark.sql(s"DROP TABLE IF EXISTS $schema.$token_table")
    spark.sql(s"CREATE TABLE IF NOT EXISTS $schema.$log_table (idx BIGINT, rowtime timestamp, date DATE, crawl_id STRING, crawl_additionalinfo MAP<String, String>, crawl_starttime timestamp, crawl_endtime timestamp, crawl_clustertype String, crawl_clustersize INT, crawl_success Boolean, crawl_status String, crawl_updated timestamp, job_id INT, run_id INT) USING DELTA PARTITIONED BY (date)")
    spark.sql(s"CREATE TABLE IF NOT EXISTS $schema.$events_table (date DATE, crawl_id STRING, crawl_event STRING, crawl_eventinfo String, crawl_eventtime timestamp) USING DELTA PARTITIONED BY (date)")
    spark.sql(s"CREATE TABLE IF NOT EXISTS $schema.$token_table (date DATE, crawl_id STRING, token_name STRING, token_state STRING, token_retries Int, token_created timestamp, token_updated timestamp) USING DELTA PARTITIONED BY (date)")
  }

  override def logCrawlStart(crawlid: String, additionalInfo: Map[String, String], clusterType: String, clusterSize: Int, jobId: Int =0 , runId: Int = 0): String = {
    val d = new java.sql.Date(System.currentTimeMillis())
    val le = StartLogEntry(new java.sql.Timestamp(System.currentTimeMillis()),d, crawlid, additionalInfo,
      Some(new Timestamp(System.currentTimeMillis())), None, Some(clusterType)
      , Some(clusterSize), None, "Scraping", new Timestamp(System.currentTimeMillis()), None, Some(runId))
    val crawlDf = Seq(le).toDF
    crawlDf.withColumn("idx", monotonically_increasing_id()).write.format("delta").mode("append").saveAsTable(schema + "." + log_table)
    "Log Added"
  }

  override def logCrawlEnd(crawlid: String, success: Boolean): String = {
    var state = "Scrape Complete"
    if(!success){
      state = "Error"
     // val env= dbutils.secrets.get(scope = "crawlconfig", key = "environment")
     // Mailgun.sendEmailAlert(env+"@spicule.co.uk", "Crawl Failed", "Crawl Failed. Crawl ID is: "+crawlid)
    }
      val le = StartLogEntry(new java.sql.Timestamp(System.currentTimeMillis()), new java.sql.Date(System.currentTimeMillis()),
        crawlid, Map(), None,Some(new Timestamp(System.currentTimeMillis())), None
        , None, Some(success), state, new Timestamp(System.currentTimeMillis()), None, None)
      val crawlDf = Seq(le).toDF
      crawlDf.withColumn("idx", monotonically_increasing_id()).write.format("delta").mode("append").saveAsTable(schema + "." + log_table)
    "Log Updated"
  }

  override def logTransformStart(crawlid: String, additionalInfo: Map[String, String], clusterType: String, clusterSize: Int): String = {
    val le = StartLogEntry(new java.sql.Timestamp(System.currentTimeMillis()), new java.sql.Date(System.currentTimeMillis()),crawlid, additionalInfo,
      Some(new Timestamp(System.currentTimeMillis())), None, Some(clusterType)
      , Some(clusterSize), None, "Transforming", new Timestamp(System.currentTimeMillis()), None, None)
    val crawlDf = Seq(le).toDF
    crawlDf.withColumn("idx", monotonically_increasing_id()).write.format("delta").mode("append").saveAsTable(schema + "." + log_table)
    "Log Added"
  }

  override def logTransformRetry(crawlid: String, additionalInfo: Map[String, String], clusterType: String, clusterSize: Int): String = {
    val le = StartLogEntry(new java.sql.Timestamp(System.currentTimeMillis()),new java.sql.Date(System.currentTimeMillis()),
      crawlid, additionalInfo, Some(new Timestamp(System.currentTimeMillis())), None, Some(clusterType)
      , Some(clusterSize), None, "Starting Retry", new Timestamp(System.currentTimeMillis()), None, None)
    val crawlDf = Seq(le).toDF
    crawlDf.withColumn("idx", monotonically_increasing_id()).write.format("delta").mode("append").saveAsTable(schema + "." + log_table)
    "Log Added"

  }


  override def logTransformEnd(crawlid: String, success: Boolean): String = {
    var state = "Transform Complete"
    val le = StartLogEntry(new java.sql.Timestamp(System.currentTimeMillis()), new java.sql.Date(System.currentTimeMillis()),
      crawlid, Map(), None,Some(new Timestamp(System.currentTimeMillis())), None
      , None, Some(success), state, new Timestamp(System.currentTimeMillis()), None, None)
    val crawlDf = Seq(le).toDF
    crawlDf.withColumn("idx", monotonically_increasing_id()).write.format("delta").mode("append").saveAsTable(schema + "." + log_table)
    "Log Updated"
  }

  override def logAdditionalCrawlEvent(crawlid: String, event: String, eventinfo: String): String = {
    val le = CrawlEvent(new java.sql.Date(System.currentTimeMillis()), crawlid, event, Some(eventinfo), new Timestamp(System.currentTimeMillis()))
    val crawlDf = Seq(le).toDF
    crawlDf.write.format("delta").mode("append").saveAsTable(schema+"."+events_table)
    "EVENT ADDED"
  }

  override def logStatusUpdate(crawlid: String, status: String): String = {
    spark.sql(s"UPDATE $schema.$log_table SET crawl_status = '$status', crawl_updated = current_timestamp() where crawl_id = '$crawlid'")
    "STATUS UPDATED"
  }

  override def seedTokenLogs(crawlid: String, tokenlist: Array[String]): String = {
    var s = Seq[(String, String, String, Int, Timestamp, Timestamp, java.sql.Date)]()
    tokenlist.foreach(t =>{
      val rowdata = (crawlid, t, "Queued", 0, new Timestamp(System.currentTimeMillis()),
        new Timestamp(System.currentTimeMillis()), new java.sql.Date(System.currentTimeMillis()))
      s = s :+ rowdata
    })

    val df = s.toDF("crawl_id", "token_name", "token_state", "token_retries", "token_created", "token_updated", "date")
    df.write.format("delta").mode("append").saveAsTable(schema+ "." + token_table)
    "TOKENS QUEUED"
  }

  override def seedTokenLogsFromFile(crawlid: String, tokenlist:String): String = {
    var s = Seq[(String, String, String, Int, Timestamp, Timestamp, java.sql.Date)]()
    val loadedtokenlist = JobAPI.fileToCleanList(tokenlist)
    loadedtokenlist.split(",").foreach(t =>{
      val rowdata = (crawlid, t, "Queued", 0, new Timestamp(System.currentTimeMillis()),
        new Timestamp(System.currentTimeMillis()), new java.sql.Date(System.currentTimeMillis()))
      s = s :+ rowdata
    })

    val df = s.toDF("crawl_id", "token_name", "token_state", "token_retries", "token_created", "token_updated")
    df.write.format("delta").mode("append").saveAsTable(schema+ "." + token_table)
    "TOKENS QUEUED"
  }

  override def updateTokenLog(crawlid: String, token: String, tokenState: String): String = {
    spark.sql(s"UPDATE $schema.$token_table SET token_state = '$tokenState' where crawl_id = '$crawlid' and token_name = '$token'")

    "TOKEN STATE UPDATED"
  }

  override def updateTokenRetryLog(crawlid: String, tokens: List[String], tokenRetry: Int) : String = {
    val utilDate = new java.util.Date()
    val df = tokens.toDF("token_name").withColumn("crawl_id", typedLit(crawlid))
      .withColumn("token_state", typedLit("Retrying")).withColumn("token_retries", typedLit(tokenRetry))
      .withColumn("token_created", typedLit(new java.sql.Timestamp(utilDate.getTime)))
      .withColumn("token_updated", typedLit( new java.sql.Timestamp(utilDate.getTime)))
      .withColumn("date", typedLit( new java.sql.Date(System.currentTimeMillis())))
    val dt = DeltaTable.forName(spark, "warehouse.token_logging")
    dt.as("tokentable").merge(
      df.as("updates"),
      "tokentable.crawl_id = '" + crawlid + "' AND tokentable.crawl_id = updates.crawl_id AND tokentable.token_name = updates.token_name AND tokentable.date = updates.date")
      .whenMatched
      .updateExpr(
        Map("token_state" -> "updates.token_state", "token_retries" -> "updates.token_retries", "token_updated" -> "updates.token_updated"))
      .whenNotMatched
      .insertAll()
      .execute()
    "TOKEN RETRY UPDATED"
  }

  override  def bulkUpdateTokenLog(crawlid: String, token: List[String], tokenState: String): String = {
    val utilDate = new java.util.Date()
    val df = token.toDF("token_name").withColumn("crawl_id", typedLit(crawlid))
      .withColumn("token_state", typedLit(tokenState)).withColumn("token_retries", typedLit(0))
      .withColumn("token_created", typedLit(new java.sql.Timestamp(utilDate.getTime)))
      .withColumn("token_updated", typedLit( new java.sql.Timestamp(utilDate.getTime)))
      .withColumn("date", typedLit( new java.sql.Date(System.currentTimeMillis())))
    val dt = DeltaTable.forName(spark, "warehouse.token_logging")
    dt.as("tokentable").merge(
      df.as("updates"),
      "tokentable.crawl_id = '" + crawlid + "' AND tokentable.crawl_id = updates.crawl_id AND tokentable.token_name = updates.token_name AND tokentable.date = updates.date")
      .whenMatched
      .updateExpr(
        Map("token_state" -> "updates.token_state"))
      .whenNotMatched
      .insertAll()
      .execute()


    "TOKEN STATE UPDATED"
  }
}
