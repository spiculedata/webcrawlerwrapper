// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package management.warehouse

import com.databricks.dbutils_v1.DBUtilsHolder.dbutils
import kong.unirest.Unirest

/**
 * Send emails via mailgun for internal use.
 */
object Mailgun {

  def sendEmail(to: String, from: String, subject: String, content: String): Unit = {
    val mgkey = dbutils.secrets.get(scope = "crawlconfig", key = "mailgunkey")
    Unirest.post("https://api.eu.mailgun.net/v3/mail.spicule.co.uk/messages")
      .basicAuth("api", mgkey)
      .field("from", from)
      .field("to", to)
      .field("subject", subject)
      .field("text", content)
      .asJson();
  }
}
