// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package management.logger

import org.apache.log4j.Logger

trait LogHelper {
  lazy val log = Logger.getLogger(this.getClass.getName)
}