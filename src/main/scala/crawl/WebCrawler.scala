// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package crawl

import crawl.databricksapi.templates.Events

import com.google.gson._
import org.apache.spark.sql.{DataFrame, SparkSession}

import java.io.{ByteArrayOutputStream, PrintWriter}
import java.nio.charset.StandardCharsets
import java.text.SimpleDateFormat
import java.util.Calendar
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.sys.process.{Process, ProcessLogger}
/**
 * WebCrawler
 * @param pSubscription The Kythera Subscription info object.
 */
class WebCrawler(seleniumUrl: String = "http://54.39.180.249:3000/webdriver",
                 installLocalSparkler :Boolean = false, bootstrap: Boolean = true, sparklerPath : String) {
  private val stampFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS")
  private var crawlStamp: String = "notspecified"
  private val randomInt = scala.util.Random
  private val localFolder = "/tmp/webcrawler/"
  val fetcherHeaders: mutable.HashMap[String, String] = new mutable.HashMap[String, String]
  val fetcherPost: mutable.HashMap[String, String] = new mutable.HashMap[String, String]
  val fetcherJSON: mutable.HashMap[String, String] = new mutable.HashMap[String, String]
  var fetcherJSONString = ""
  val fetcherReplace: mutable.HashMap[String, String] = new mutable.HashMap[String, String]
  val fetcherActivePlugins: mutable.ArrayBuffer[String] = new mutable.ArrayBuffer[String]
  val fetcherChrome: mutable.HashMap[String, String] = new mutable.HashMap[String, String]
  val fetcherRegex: mutable.HashMap[String, Object] = new mutable.HashMap[String, Object]
  val databricksApi: mutable.HashMap[String, Object] = new mutable.HashMap[String, Object]
  val fetcherValues: mutable.ArrayBuffer[String] = new mutable.ArrayBuffer[String]
  var fetcherTag = ""
  var start = 1000
  var end = 3000
  val rnd = new scala.util.Random
  val delay: Int = start + rnd.nextInt((end - start) + 1)
  var repartitioncount = 1
  var javascriptready = false

  case class seleniumElement(operation: String, value: String)

  val fetcherSelenium: mutable.ArrayBuffer[seleniumElement] = new mutable.ArrayBuffer[seleniumElement]
  var fetcherFullSelenium: JsonObject = new JsonObject
  var contentpath = ""
  var contentFileName = ""
  var failurepercent = None: Option[Double]
  var chromeoutputlocation = "/dbfs/FileStore/crawl/content/"
  var crawlDepth = 1
  var crawlWidth = 5000
  var crawlSolrDbURL = "http://ec2-35-174-200-133.compute-1.amazonaws.com:8983/solr/crawldb"
  var masterIP = "spark://127.0.0.1:7077"
  var screenshotoncapture = false

  println("Preparing cluster for WebCrawl operations")
  private val cmdExists = Process("[ -e " + localFolder + " ]").!
  if (bootstrap && cmdExists != 0) {
    Process("pip --disable-pip-version-check install databricks-cli").!
    Process("mkdir " + localFolder).!

    //Now find the IP address that spark port is on
    val netstatOutput = ListBuffer.empty[String]
    val netstatLogger = new ProcessLogger {
      override def out(s: => String): Unit = {
        if (s.trim.contains(":7077")) {
          var tempString = s.substring(0, s.indexOf(":7077")).trim
          tempString = tempString.substring(tempString.lastIndexOf(" ")).trim
          netstatOutput.append(tempString)
        }
      }

      override def err(s: => String): Unit = {
        if (s.trim.nonEmpty) {
          println("netstat-error: " + s)
        }
      }

      override def buffer[T](f: => T): T = f
    }
    Process("netstat -tulpn").!(netstatLogger)
    for (line <- netstatOutput) {
      masterIP = line.trim
    }

    if(installLocalSparkler){
      val u = new Utils
      u.installSparkler(localFolder, masterIP, crawlSolrDbURL, sparklerPath)
    }


  }
  else {
    println("Cluster already configured for WebCrawl operations powered by the amazing Sparkler engine")
  }
  resetFetcherHeaders
  resetFetcherActivePlugins

  def setDelay(s: Int, e: Int) {
    start = s
    end = e
  }

  def setRepartitionCount(c: Int): Unit ={
    repartitioncount = c
  }

  def setSparkMaster(s: String) : Unit ={
    masterIP = s
  }

  /** *******************************
   * Start of Function definitions
   * ******************************* */
  /**
   * Reset the fetcher headers to sensible defaults.
   * @return
   */
  def resetFetcherHeaders: Any = {
    fetcherHeaders.clear()
    //fetcherHeaders.put("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Sparkler/0.2.1-SNAPSHOT")
    fetcherHeaders.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
    fetcherHeaders.put("Accept-Language", "en-US,en")
  }

  /**
   * Set the fetcher headers
   * @param headers a tuple list of headers to pass the crawl
   * @return
   */
  def setFetcherHeaders(headers: Seq[(String, String)]): Any = {
    headers.foreach(tuple => fetcherHeaders.put(tuple._1, tuple._2))
  }

  /**
   * Reset the fetcher post
   * @return
   */
  def resetFetcherPost(): Any = {
    fetcherPost.clear()
  }

  /**
   * Reset the fetcher JSON
   * @return
   */
  def resetFetcherJSON(): Any = {
    fetcherJSON.clear()
  }

  /**
   * Reset the fetcher replace
   * @return
   */
  def resetFetcherReplace(): Any = {
    fetcherReplace.clear()
  }

  /**
   * Reset the default active plugins
   * @return
   */
  def resetFetcherActivePlugins: Any = {
    fetcherActivePlugins.clear()
    fetcherActivePlugins += "urlfilter-regex"
    fetcherActivePlugins += "urlfilter-samehost"
    fetcherActivePlugins += "url-injector"
  }

  /**
   * Reset the fetcher values
   * @return
   */
  def resetFetcherValues(): Any = {
    fetcherValues.clear()
  }

  /**
   * Set the fetcher values from a dataframe
   * @param listDataframe dataframe of values
   * @return
   */
  def setFetcherValues(listDataframe: DataFrame): Int = {
    fetcherValues.clear()
    val valuesList = listDataframe.rdd.map(valueRow => valueRow.getString(0)).collect
    valuesList.foreach(theValue => {
      fetcherValues += theValue
    })
    fetcherValues.size
  }

  /**
   * Reset fetcher selenium
   * @return
   */
  def resetFetcherSelenium(): Any = {
    fetcherSelenium.clear()
  }

  /**
   * Add a selenium lookup element
   * @param element a seleniumElement object
   * @return
   */
  def setFetcherSeleniumElement(element: seleniumElement): Int = {
    fetcherSelenium += element
    fetcherSelenium.size
  }

  def setFetcherSeleniumScript(script: JsonObject): Any = {
    fetcherFullSelenium = script
  }

  /**
   * Remove the fetcherchrome plugin
   * @return
   */
  def removeFetcherChrome(): Any = {
    fetcherChrome.clear()
    fetcherActivePlugins -= "fetcher-chrome"
  }

  def removeUrlInjector(): Any = {
    fetcherActivePlugins -= "url-injector"
  }

  def waitForJavascript(wait: Boolean): Any = {
    this.javascriptready = wait
  }

  def setContentPath(path: String) : Any = {
    this.contentpath = path
  }

  def setContentFileName(name: String = "hash") : Any = {
    this.contentFileName = name
  }

  def setfailurePercent(pct: Double) : Any = {
    this.failurepercent = Option(pct)
  }

  def setChromeOutputLocation(loc: String) : Any = {
    this.chromeoutputlocation = loc
  }

  def setChromeScreenshotOutput(trigger: Boolean = true) : Any = {
    this.screenshotoncapture = trigger
  }
  /**
   * Set the fetcher chrome plugin.
   * @return
   */
  def setFetcherChrome(): Any = {
    removeFetcherChrome()
    fetcherChrome.put("chrome.dns", seleniumUrl)
    fetcherChrome.put("chrome.proxy.address", "us-wa.proxymesh.com:31280,us-il.proxymesh.com:31280,us.proxymesh.com:31280,us-dc.proxymesh.com:31280,us-ca.proxymesh.com:31280,us-ny.proxymesh.com:31280,us-fl.proxymesh.com:31280")

    fetcherChrome.put("chrome.selenium.javascriptready", javascriptready.toString)
    fetcherChrome.put("chrome.selenium.outputdirectory", chromeoutputlocation)
    fetcherActivePlugins += "fetcher-chrome"
  }

  /**
   * Set the fetcher chrome plugin settings
   * @param setting a setting string
   * @param value the setting value
   * @return
   */
  def setFetcherChrome(setting: String, value: String): Any = {
    fetcherChrome.put(setting, value)
    fetcherActivePlugins += "fetcher-chrome"
  }

  def setFetcherRegex(values : List[String], includedefaults : Boolean = true): Unit ={
    if(includedefaults) {
      fetcherRegex.put("urlfilter.regex.file", "regex-urlfilter.txt")
    }
    fetcherRegex.put("urlfilter.regex.items", values)

  }


  def setDatabricksAPI(events: Events): JsonObject = {
    fetcherActivePlugins += "url-injector"

    val parent = new JsonObject

    val gson = new Gson();
    val json = gson.toJson(events);
    val parser = new JsonParser()
    parser.parse(json).getAsJsonObject

  }

  /**
   * Build the configuration JSON file.
   * @return
   */
  def buildConfigurationJson(mode :String = "development", databricksTemplate: Option[Events] = None): String = {
    val sparklerConfiguration = new JsonObject

    sparklerConfiguration.addProperty("crawldb.uri", crawlSolrDbURL)
    sparklerConfiguration.addProperty("spark.master", masterIP)
    sparklerConfiguration.addProperty("databricks.enable", "true")
    sparklerConfiguration.addProperty("generate.topn", 1000)
    sparklerConfiguration.addProperty("generate.top.groups", 256)
    sparklerConfiguration.addProperty("generate.sortby", "discover_depth asc, score asc")
    sparklerConfiguration.addProperty("generate.groupby", "group")
    sparklerConfiguration.addProperty("fetcher.server.delay", delay)
    sparklerConfiguration.addProperty("crawl.repartition", repartitioncount)
    sparklerConfiguration.addProperty("fetcher.proxy.url", "us-wa.proxymesh.com")
    sparklerConfiguration.addProperty("fetcher.proxy.port", "31280")
    if(failurepercent.isDefined){
      sparklerConfiguration.addProperty("fetcher.kill.failure.percent", failurepercent.get)
    }
    sparklerConfiguration.addProperty("fetcher.persist.content.filename", contentFileName)
    val jsonFetcherHdr = new JsonObject
    for (headerField <- fetcherHeaders.keySet) {
      if (headerField.contains("timeout")) {
        jsonFetcherHdr.addProperty(headerField, fetcherHeaders(headerField).toInt)
      }
      else {
        jsonFetcherHdr.addProperty(headerField, fetcherHeaders(headerField))
      }
    }
    sparklerConfiguration.add("fetcher.headers", jsonFetcherHdr)

    val jsonActivePlugins = new JsonArray
    for (theActPlug <- fetcherActivePlugins) {
      jsonActivePlugins.add(new JsonPrimitive(theActPlug))
    }
    sparklerConfiguration.add("plugins.active", jsonActivePlugins)
    val pluginOptions = getPluginOptions
    if(mode == "production"){
      if(databricksTemplate.nonEmpty){
        val template = databricksTemplate.get
        val events = new JsonObject()
        events.add("events", setDatabricksAPI(template))
        pluginOptions.add("databricks.api", events)
      }
    }
    sparklerConfiguration.add("plugins", pluginOptions)
    print("BUILT CONFIGURATION: "+sparklerConfiguration.toString)
    import java.util.Base64
    val bytes = Base64.getEncoder().encodeToString(sparklerConfiguration.toString.getBytes(StandardCharsets.UTF_8))
    bytes
  }

  /**
   * Get Plugin Options
   * @return
   */
  private def getPluginOptions: JsonObject = {
    val fetcherPlugins = new JsonObject
    if(fetcherRegex.nonEmpty){
      val jsonFetcherRegex = new JsonObject
      for (regexField <- fetcherRegex.keySet) {
        val o = fetcherRegex(regexField)
        if(o.isInstanceOf[List[_]]){
          val jsonValues = new JsonArray
          for (theValue <- o.asInstanceOf[List[String]]) {
            jsonValues.add(new JsonPrimitive(theValue))
          }
        } else{
          jsonFetcherRegex.addProperty(regexField, fetcherRegex(regexField).toString)
        }
      }
      fetcherPlugins.add("urlfilter.regex", jsonFetcherRegex)
    }
    if (fetcherChrome.nonEmpty || fetcherSelenium.nonEmpty || fetcherPost.nonEmpty || fetcherJSON.nonEmpty ||
      fetcherJSONString != "" || fetcherReplace.nonEmpty) {
      if (fetcherChrome.nonEmpty || fetcherSelenium.nonEmpty) {
        if (fetcherChrome.isEmpty) {
          setFetcherChrome()
        }
        val jsonFetcherChrome = new JsonObject
        for (chromeField <- fetcherChrome.keySet) {
          jsonFetcherChrome.addProperty(chromeField, fetcherChrome(chromeField))
        }
        fetcherPlugins.add("fetcher.chrome", jsonFetcherChrome)

        val jsonUrlInjector = new JsonObject
        jsonUrlInjector.addProperty("mode", "selenium")

        print("FETCHER SIZE: " + fetcherValues.size)
        val jsonValues = new JsonArray
        for (theValue <- fetcherValues) {
          jsonValues.add(new JsonPrimitive(theValue))
        }
        println("JSON SIZE: " + jsonValues.size)
        jsonUrlInjector.add("values", jsonValues)

        var jsonFetcherSelenium = new JsonObject
        var seleniumIndex = 1
        //Either set line level selenium attributes or pass a full json selenium object and use that instead
        //val env = subscription.getEnvironment
        if(fetcherSelenium.isEmpty && !fetcherFullSelenium.isJsonNull) {
          //env.logger.printToLog("Inserting full selenium object")
          println("Inserting full selenium object")
          jsonFetcherSelenium = fetcherFullSelenium
        } else {
          for (element <- fetcherSelenium) {
            //env.logger.printToLog("Inserting Selenium Object")
            println("Inserting Selenium Object")
            val jsonSeleniumElement = new JsonObject
            jsonSeleniumElement.addProperty("operation", element.operation)
            jsonSeleniumElement.addProperty("value", element.value)
            jsonFetcherSelenium.add(seleniumIndex.toString, jsonSeleniumElement)
            seleniumIndex += 1
          }
        }
        jsonUrlInjector.add("selenium", jsonFetcherSelenium)
        jsonUrlInjector.add("tag", new JsonPrimitive(fetcherTag))
        fetcherPlugins.add("url.injector", jsonUrlInjector)
      }

      if (fetcherPost.nonEmpty) {
        val jsonUrlInjector = new JsonObject
        jsonUrlInjector.addProperty("mode", "form")

        val jsonFetcherPost = new JsonObject
        val jsonValues = new JsonArray
        for (theValue <- fetcherValues) {
          jsonValues.add(new JsonPrimitive(theValue))
        }
        jsonUrlInjector.add("values", jsonValues)
        println("JSON POST SIZE: " + jsonValues.size)
        //jsonFetcherPost.add("values", jsonValues)

        for (postField <- fetcherPost.keySet) {
          jsonFetcherPost.addProperty(postField, fetcherPost(postField))
        }
        jsonUrlInjector.add("form", jsonFetcherPost)
        jsonUrlInjector.add("tag", new JsonPrimitive(fetcherTag))

        fetcherPlugins.add("url.injector", jsonUrlInjector)
      }
      if (fetcherJSON.nonEmpty) {
        val jsonUrlInjector = new JsonObject
        jsonUrlInjector.addProperty("mode", "json")

        val jsonJSONPost = new JsonObject
        val jsonValues = new JsonArray
        for (theValue <- fetcherValues) {
          jsonValues.add(new JsonPrimitive(theValue))
          jsonUrlInjector.add("values", jsonValues)
        }
        println("JSON JSON SIZE: " + jsonValues.size)
        for (jsonField <- fetcherJSON.keySet) {
          //jsonJSONPost.addProperty(jsonField, fetcherJSON(jsonField))
        }
        jsonUrlInjector.add("json", new JsonPrimitive("${token}"))
        jsonUrlInjector.add("tag", new JsonPrimitive(fetcherTag))
        fetcherPlugins.add("url.injector", jsonUrlInjector)
      }
      if (fetcherJSONString != "") {
        print("FETCHER JSON STRING SET")
        val jsonUrlInjector = new JsonObject
        jsonUrlInjector.addProperty("mode", "json")
        val jsonValues = new JsonArray
        for (theValue <- fetcherValues) {
          jsonValues.add(new JsonPrimitive(theValue))
          jsonUrlInjector.add("values", jsonValues)
        }
        jsonUrlInjector.add("json", new JsonPrimitive(fetcherJSONString))
        jsonUrlInjector.add("tag", new JsonPrimitive(fetcherTag))
        fetcherPlugins.add("url.injector", jsonUrlInjector)
      }
      if (fetcherReplace.nonEmpty) {
        val jsonUrlInjector = new JsonObject
        jsonUrlInjector.addProperty("mode", "replace")
        fetcherPlugins.add("url.injector", jsonUrlInjector)
        val jsonValues = new JsonArray
        for (theValue <- fetcherValues) {
          jsonValues.add(new JsonPrimitive(theValue))
        }
        jsonUrlInjector.add("tag", new JsonPrimitive(fetcherTag))
        println("JSON REPLACE SIZE: " + jsonValues.size)
        jsonUrlInjector.add("values", jsonValues)
      }
    }

    print(fetcherPlugins)
    fetcherPlugins
  }

  /**
   * Compile the crawl command
   * @param jobId the jobid value
   * @return
   */
  def getCrawlCommand(jobId: String): Seq[String] = {
    Seq[String](
      localFolder + "sparkler/bin/sparkler.sh"
    ) ++ getCrawlArgs(jobId)
    //localFolder + "sparkler/bin/sparkler.sh crawl -id " + jobId + " -tn " + crawlWidth + " -i " + crawlDepth + " -co '" + getCrawlCommandOptions + "'"
  }

  def getCrawlArgs(jobId: String, environment :String = "development", databricksTemplate: Option[Events] = None, idfile : String = ""): Seq[String] = {
    var idf = "-id"
    var jid = jobId
    if(idfile.nonEmpty){
      idf = "-idf"
      jid = idfile
    }
    Seq[String](
      "crawl",
      idf,
      jid,
      "-tn",
      crawlWidth.toString,
      "-i",
      crawlDepth.toString,
      "-co64",
      buildConfigurationJson(environment, databricksTemplate)
    )
  }
  /**
   * Get the inject command
   * @param jobId the job id
   * @param inDomainList the list of domains
   * @return
   */
  def getInjectCommand(jobId: String, inDomainList: DataFrame, seedfile: String): Seq[String] = {
    Seq[String](
      localFolder + "sparkler/bin/sparkler.sh",
    ) ++ getInjectArgs(jobId, inDomainList, seedfile)
  }


  def configToSeed(crawlid: String): String ={
    val u = new Utils
    var confjson = buildConfigurationJson()
    import java.util.Base64
    confjson = new String(Base64.getDecoder.decode(confjson))
    val filename = "/dbfs/FileStore/seeds/"+crawlid+"config.txt"
    u.writeFile(filename, List(confjson))
    filename
  }
  def listToUrlArgs(urllist: String): Seq[String] = {
    val list = urllist.split(",")
    val lb = new ListBuffer[String]
    list.foreach(f => {
      lb += "-su"
      lb += f.trim
    })
    lb.toList
  }
  def getInjectArgs(jobId: String, inDomainList: DataFrame, seedfile: String, configtofile: Boolean = false, idfile :String = ""): Seq[String] = {
    var idflag = "-id"
    var jid = jobId
    if(idfile.nonEmpty){
      idflag = "-idf"
      jid = idfile
    }
    if(seedfile == null) {
      val urllist = inDomainList.select("DOMAIN").rdd.map(r => r(0)).collect.mkString(",")
      val sus = listToUrlArgs(urllist)
      if(!configtofile) {
        Seq[String](
          "inject",
          idflag,
          jid) ++ sus ++ Seq[String](
          "-co64",
          buildConfigurationJson()
        )
      } else {
        val filename = configToSeed(jobId)
        Seq[String](
          "inject",
          idflag,
          jid) ++ sus ++ Seq[String](
          "-cof",
          filename
        )
      }
    } else{
      if(!configtofile) {
        Seq[String](
          "inject",
          idflag,
          jid,
          "-sf",
          seedfile,
          "-co64",
          buildConfigurationJson())
      } else{
        val filename = configToSeed(jobId)
        Seq[String](
          "inject",
          idflag,
          jid,
          "-sf",
          seedfile,
          "-cof",
          filename)
      }
    }
  }
  /**
   * Inject the data
   * @param crawlId the crawl id
   * @param inDomainList the domain list
   * @return
   */
  def inject(crawlId: String, inDomainList: DataFrame, seedfile: String = null): Int = {
    crawlStamp = stampFormat.format(Calendar.getInstance().getTime)

    var jobId = crawlId
    if (crawlId == null || crawlId.trim.isEmpty) {
      jobId = "j" + randomInt.nextInt(100)
    }

    println(getInjectCommand(jobId, inDomainList, seedfile))
    Process(getInjectCommand(jobId, inDomainList, seedfile)).!
  }


  import sys.process._
  def runCommand(cmd: Seq[String]): (Int, String, String) = {
    val stdoutStream = new ByteArrayOutputStream
    val stderrStream = new ByteArrayOutputStream
    val stdoutWriter = new PrintWriter(stdoutStream)
    val stderrWriter = new PrintWriter(stderrStream)
    val exitValue = cmd.!(ProcessLogger(stdoutWriter.println, stderrWriter.println))
    stdoutWriter.close()
    stderrWriter.close()
    (exitValue, stdoutStream.toString, stderrStream.toString)
  }

  /**
   * Run the crawl
   * @param crawlId the crawl id
   * @param depth the crawl depth
   * @return
   */
  def crawl(crawlId: String, depth: Int = 1): Int = {

    crawlDepth = depth
    crawlStamp = stampFormat.format(Calendar.getInstance().getTime)

    var jobId = crawlId
    if (crawlId == null || crawlId.trim.isEmpty) {
      jobId = "j" + randomInt.nextInt(100)
    }

    println(getCrawlCommand(jobId))

    Process(getCrawlCommand(jobId)).!
  }

  /**
   * Crawl and persist the output to databricks
   * @param crawlId the crawl id
   * @param inDomainList the domain list
   * @param tableName the databricks table name
   * @param depth the crawl depth
   * @return
   */
  def crawlAndPersist(crawlId: String, tableName: String, depth: Int = 1): Int = {
    //inject(crawlId, inDomainList)
    val crawlReturn = crawl(crawlId, depth)
    val p = new Persistence
    val spark = SparkSession.builder.getOrCreate()
    p.persistResults(crawlId, tableName, "warehouse_sandbox", spark, crawlSolrDbURL, "")
    crawlReturn
  }

}
