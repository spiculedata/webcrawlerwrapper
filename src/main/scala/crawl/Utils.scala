// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package crawl

import com.databricks.dbutils_v1.DBUtilsHolder.dbutils
import com.google.gson.{Gson, JsonObject}
import com.kytheralabs.management.logger.LogHelper
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{DataFrame, SparkSession}

import java.io.{BufferedWriter, File, FileWriter}
import scala.collection.mutable.ListBuffer
import scala.sys.process.{Process, ProcessLogger, _}

/**
 * Generic utilities
 */
class Utils extends LogHelper {

  private val crawlOutput = ListBuffer.empty[String]

  /**
   * Write lines to a file.
   * @param filename the filename
   * @param lines the lines to write
   */
  def writeFile(filename: String, lines: Seq[String]): Unit = {
    val file = new File(filename)
    val bw = new BufferedWriter(new FileWriter(file))
    for (line <- lines) {
      bw.write(line + "\n")
    }
    bw.close()
  }

  /**
   * Check for sparkler being installed
   * @param version the version that should be installed
   * @return
   */
  def checkForSparkler(version: String): (Boolean,String) ={
    val jarexists = new java.io.File("/dbfs/FileStore/release/sparkler-app-" + version + ".jar").exists
    val zipexists = new java.io.File("/dbfs/FileStore/release/sparkler-app-" + version + ".zip").exists
    if(!jarexists && !zipexists){
      (false, "Neither zip nor jar detected for sparkler version: " + version)
    } else if(!jarexists){
      (false, "Jar detected for sparkler version: " + version)
    } else if(!zipexists){
      (false, "Zip not detected for sparkler version: " + version)
    } else {
      (true, "Sparkler found")
    }
  }

  /**
   * Convert yaml to json
   * @param json the yaml
   * @return
   */
  def yamltojson(json: String): String ={
    import org.yaml.snakeyaml.Yaml
    val yaml = new Yaml
    val map: java.util.Map[String, Object] = yaml.load(json)

    val gson = new Gson()
    gson.toJson(map)
  }

  /**
   * Log process output
   */
  private val processLogger = new ProcessLogger {
    override def out(s: => String): Unit = {
      if (s.trim.nonEmpty) {
        crawlOutput.append("wayfinder-output: " + s)
      }
    }

    override def err(s: => String): Unit = {
      if (s.trim.nonEmpty) {
        crawlOutput.append("wayfinder-error: " + s)
      }
    }

    override def buffer[T](f: => T): T = f
  }

  def extractRunID(response: String) : Int = {
    val gson = new Gson()
    val json = gson.fromJson(response, classOf[JsonObject])
    json.get("run_id").getAsInt
  }
  /**
   * Install Sparkler
   * @param subscription the kythera subscription
   * @param localFolder the local folder
   * @param masterIP the spark master ip
   * @param solrdburl the solrdb url
   * @param path the path
   */
  def installSparkler(localFolder: String, masterIP: String, solrdburl: String, path: String): Unit ={
    var crawlSolrDbURL = solrdburl
    log.info("Copying and installing sparkler.zip")
    log.info("FETCHING SPARKLER.ZIP from:  dbfs:/FileStore/sparkler-zip/sparkler.zip")
    //DatabricksUtilities.callCli(subscription, "cp "+path+" " + localFolder + "sparkler.zip")
    //dbutils.fs.cp(path, localFolder+"sparkler.zip")
    val theBashFilename = localFolder + "wayfinder_webcrawler.bash"
    val commandFile = new File(theBashFilename)
    val fileWriter = new BufferedWriter(new FileWriter(commandFile))

    fileWriter.write("cp " + path + " "+localFolder+"sparkler.zip\n")
    fileWriter.write("cd " + localFolder + " \n")
    fileWriter.write("unzip -q sparkler.zip -d sparkler\n")
    fileWriter.write("chmod -R 770 " + localFolder + " \n")
    fileWriter.write("echo Configuring sparkler to connect to spark on " + masterIP + " \n")
    fileWriter.write("sed -i 's/\\[MASTER_IP_ADDRESS\\]/" + masterIP + "/g' " + localFolder + "sparkler/conf/sparkler-default.yaml \n")
    fileWriter.write("grep crawldb.uri " + localFolder + "sparkler/conf/sparkler-default.yaml \n")

    fileWriter.close()

    Process("chmod 777 " + theBashFilename).!

    //Process("cat " + localFolder + "wayfinder_webcrawler.bash").!(processLogger)
    Process(theBashFilename).!(processLogger)
    for (line <- crawlOutput) {
      if (line.contains("crawldb.uri")) {
        val tempLine = line.replace("wayfinder-output:", "").replace("crawldb.uri:", "").trim
        if (!tempLine.startsWith("#")) {
          crawlSolrDbURL = tempLine
          log.info("crawlSolrDbURL = " + crawlSolrDbURL)
        }
      }
      else {
        log.info(line)
      }
    }
    crawlOutput.clear()

    log.info("Cluster ready for WebCrawl operations powered by the amazing Sparkler engine")
  }

  /**
   * Conditionall fail a job after a certain amount of failed runs
   * @param spark the notebook spark object
   * @param agentname the agent name
   */
  def conditionalJobDisable(spark: SparkSession, agentname: String): Unit ={
    //Look up the outcome of the past jobs
    var df = spark.table("warehouse.crawl_logging")
    df = df.where(col("crawl_additionalinfo").getItem("agentname") === agentname)
    df = df.orderBy(col("crawl_endtime").desc).limit(3)

    //If greater than the required amount
    val a = df.rdd.map(r => r(0)).collect()

    val same = a.forall(_ == a.head)
    if(same && a(0) == false){
      // TODO NO SCHEDULER TO FAIL YET
    }

  }

  /**
   * Parse a google sheet to dataframe
   * @param spark the notebook spark object
   * @param localpath the local path
   * @param url the url to get the sheet from
   * @param tableName the table name
   */
  def parseGoogleSheet(spark: SparkSession, fileName: String, url: String): DataFrame ={
    dbutils.fs.rm("file:/tmp/" + fileName)
    "wget -O /tmp/" + fileName + " " + url !!

    // store the test criteria in DBFS
    dbutils.fs.mkdirs("dbfs:/datasets/gsheets")
    dbutils.fs.cp("file:/tmp/" + fileName, "dbfs:/datasets/gsheets")

    // Convert the CSV to a DataFrame
    val df = spark.read.option("header", "true").option("inferSchema", "true").csv("/datasets/gsheets/" + fileName)

    df
  }
}
