// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package crawl.templates

import crawl.WebCrawler
import management.jobutils.TasksHolder
import management.{Crawl, JavaCrawl}

import com.google.gson.Gson
import org.apache.spark.sql.SparkSession

import java.nio.charset.StandardCharsets
import java.time.LocalDateTime
import java.util.Base64
import scala.collection.JavaConverters._
import scala.collection.mutable


class ReplaceCrawlScheduled(spark: SparkSession, agentName: String, sparklerVersion: String)
  extends ReplaceCrawl(spark = spark, sparklerVersion = sparklerVersion, agentName = agentName) {


  /**
   * Run a production crawl
   * @param availableparams the parameters available in this crawl
   * @param headers the headers you want to inject
   * @param c the config option
   * @return
   */
  override def runProductionCrawl(availableparams: Map[String, String], headers: Seq[(String, String)], c: Option[Crawl],
                                 injectargs: Option[Seq[String]], crawldir: String): String ={
    val webCrawler = new WebCrawler(seleniumUrl="local", installLocalSparkler =  false, sparklerPath = sparklerpath)
    log.info("Configuring post crawl transform")
    webCrawler.setFetcherHeaders(headers)
    val e = generateTransformTemplate(availableparams, c, crawldir)
    val args = webCrawler.getCrawlArgs(availableparams("crawlid"), "production", Some(e))

    log.info("Calling submit job")
    val crawlsize = setProdOptions(webCrawler, availableparams, c)

    val clustersize: mutable.HashMap[String, String] = if (injectargs.isDefined) {
      mutable.HashMap("inject" -> "1", "crawl" -> crawlsize.toString)
    } else {
      mutable.HashMap("crawl" -> crawlsize.toString)
    }
    val tasks: mutable.HashMap[String, java.util.List[String]] = if (injectargs.isDefined) {
      mutable.HashMap("inject" -> injectargs.get.toList.asJava, "crawl" -> args.toList.asJava)
    } else {
      mutable.HashMap("crawl" -> args.toList.asJava)
    }

    val formulary = c.get.config("formularyid")

    val gson = new Gson()
    val jcrawl = JavaCrawl(c.get.name, c.get.path, c.get.config.asJava, c.get.transformPath.get, c.get.state)
    val task = TasksHolder(availableparams("crawlid"), formulary, tasks.asJava, getPluginDir, clustersize.asJava, getSparklerVersion, jcrawl, e)

    val tson = gson.toJson(task)
    val cid = availableparams("crawlid")
    val ts = LocalDateTime.now()
    val encode = Base64.getEncoder.encodeToString(tson.getBytes(StandardCharsets.UTF_8))
    spark.sql(s"""insert into warehouse.crawl_queue values("$agentName", "$cid", "$encode", "$ts", "$formulary")""")
    log.info("crawl queued")
    "Queued"
  }



}
