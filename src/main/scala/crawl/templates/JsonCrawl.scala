// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package crawl.templates

import crawl.{Helper, WebCrawler}
import crawl.databricksapi.templates.{Events, Shutdown, Triggerjob}
import management.{Crawl, ManagementCore}
import management.jobutils.JobAPI

import com.databricks.dbutils_v1.DBUtilsHolder.dbutils
import org.apache.spark.sql.SparkSession

import scala.collection.JavaConversions._

/**
 * A JSON Crawl
 * @param spark the spark object from the notebook
 * @param agentName the agent name
 * @param sparklerVersion the sparkler version
 */
class JsonCrawl (spark: SparkSession, agentName: String, sparklerVersion : String)
  extends CoreTemplate(spark = spark, sparklerVersion = sparklerVersion) {
  import spark.implicits._

  /**
   * Initialise the crawl
   * @param url the desired url
   * @param availableparams the available parameters for the crawl.
   */
  private def initCrawl(url: String, availableparams: Map[String, String]): Unit ={
    val inputDataframeOfURLS = Seq(
      url
    ).toDF("DOMAIN")
    //This is development mode, production needs to do this in sparksubmit
    val webCrawler = new WebCrawler( "local", installLocalSparkler = true, sparklerPath = sparklerpath)
    webCrawler.fetcherJSON.put("enable", "plugin")
    webCrawler.inject(availableparams("crawlid"), inputDataframeOfURLS.select('DOMAIN))
  }

  /**
   * Run the crawl
   * @param url the url to crawl
   * @param headers the headers required for the crawl
   * @param injectonly whether to inject only or run a full crawl
   * @return
   */
  def runCrawl(url: String, headers: Seq[(String, String)],  injectonly : Boolean = false) : String = {

    val availableparams = getParams

    checkParams(availableparams)

    initCrawl(url, availableparams)
    if(!injectonly) {
      executeCrawl(availableparams, headers)
    } else{
      "Inject Only Job"
    }

  }

  /**
   * Execute a crawl
   * @param availableparams the parameters available in the crawl
   * @param headers the defined headers for the crawl
   * @return
   */
  private def executeCrawl(availableparams: Map[String, String], headers: Seq[(String,String)]): String = {
    log.info("Running Crawl")
    if(availableparams("executiontype") == "Development") {
      runDevelopmentCrawl(availableparams, headers)
    } else {
      //run spark submit
      log.info("Launching in production mode")
      val m = new ManagementCore(spark)
      val c = m.getCrawl(agentName + "_" + dbutils.widgets.get("formularyid"))
      if (c.isEmpty) {
        log.info("Failed, no config found")
        "Failed, crawl not found in the database"
      } else {
        runProductionCrawl(availableparams, headers, c)
      }
    }
  }

  /**
   * Run a development crawl
   * @param availableparams the parameters available in the crawl
   * @param headers the defined headers for the crawl
   * @return
   */
  private def runDevelopmentCrawl(availableparams: Map[String, String], headers: Seq[(String,String)]): String ={
    val webCrawler = new WebCrawler("local", true, sparklerPath = sparklerpath)
    webCrawler.setFetcherHeaders(headers)
    webCrawler.fetcherJSON.put("enable", "plugin")
    webCrawler.buildConfigurationJson()
    log.info("Launching in development mode")
    val i = webCrawler.crawl(availableparams("crawlid"))
    i.toString
  }

  /**
   * Run a production crawl
   * @param availableparams the available parameters in the crawl
   * @param headers the defined headers
   * @param c the crawl config
   * @return
   */
  private def runProductionCrawl(availableparams: Map[String, String], headers: Seq[(String,String)], c: Option[Crawl]) : String = {
    n.logCrawlStart(availableparams("crawlid"), c.get.config, c.get.config("clustertype"), c.get.config("clustersize").toInt,jobId = 0,runId = 0)
    val webCrawler = new WebCrawler(seleniumUrl = "local", installLocalSparkler = false, sparklerPath = sparklerpath)
    log.info("Configuring post crawl transform")
    //Only need 1 node for the transform
    val e = new Events
    val shutdown = new Shutdown
    val tj = new Triggerjob

    var baseUrl = ""
    if(availableparams.contains("baseurl")) {
      baseUrl = availableparams("baseurl")
    }
    else {
      baseUrl = Helper.getBaseUrl()
    }

    tj.setApiKey(dbutils.secrets.get("crawlconfig", "apikey"))
    tj.setApiURL(dbutils.secrets.get("crawlconfig", "apiurl"))
    tj.setNotebook(baseUrl + c.get.transformPath.get)
    tj.setSparkversion("7.3.x-scala2.12")
    tj.setInstancetype(c.get.config("clustertype"))
    tj.setClustersize(1)
    val transformparams = Map("agentname" -> c.get.config("agentname"), "crawlid" -> availableparams("crawlid"),
      "debug" -> "false", "formularyid" -> availableparams("formularyid"), "qa" -> "false", "tokentype" -> "3", "baseurl" -> baseUrl)
    val jmap = new java.util.HashMap[String, String](transformparams)
    tj.setParameters(jmap)
    shutdown.setTriggerjob(tj)
    e.setShutdown(shutdown)
    val repCount = 500
    webCrawler.setRepartitionCount(repCount)
    webCrawler.setFetcherHeaders(headers)
    webCrawler.fetcherJSON.put("enable", "plugin")

    webCrawler.setSparkMaster("")
    val args = webCrawler.getCrawlArgs(availableparams("crawlid"), "production", Some(e))

    val clustersize = c.get.config("clustersize").toInt

    log.info("Calling submit job")
    JobAPI.runSparkSubmit(c.get.config("scalaversion"), c.get.config("clustertype"), clustersize,
      availableparams("crawlid"), "prod", "edu.usc.irds.sparkler.Main",
      submitpath, args.toList,getPluginDir, c.get.config("cpus"), c.get.config("drivermemory"), c.get.config("executormemory"))
  }
}
