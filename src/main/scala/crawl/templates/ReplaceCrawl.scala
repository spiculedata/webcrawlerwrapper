// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package crawl.templates

import crawl.{Utils, WebCrawler}
import management.{Crawl, ManagementCore}
import management.jobutils.JobAPI

import com.databricks.dbutils_v1.DBUtilsHolder.dbutils
import org.apache.spark.sql.SparkSession

import java.net.URLEncoder


/**
 * A replace style crawl
 * @param spark the notebook spark object
 * @param agentName the agent name
 * @param sparklerVersion the sparkler version
 */


class ReplaceCrawl (spark: SparkSession, agentName: String, sparklerVersion : String)
  extends CoreTemplate(spark = spark, sparklerVersion = sparklerVersion) {
  import spark.implicits._

  /**
   * Initialise a crawl
   * @param url the url you want to crawl
   * @param availableparams the parameters available in this crawl
   */
  private def initCrawl(url: String, availableparams: Map[String, String], tokenlist: Array[String]): Option[Seq[String]] ={
    val inputDataframeOfURLS = Seq(
      (url)
    ).toDF("DOMAIN")
    val u = new Utils
    //This is development mode, production needs to do this in sparksubmit
    val webCrawler = new WebCrawler("local", installLocalSparkler = true, sparklerPath = sparklerpath)
    webCrawler.setFetcherValues(tokenlist.toList.toDF())
    webCrawler.fetcherReplace.put("enable","plugin")
    if (availableparams("executiontype") == "Development") {
      webCrawler.inject(availableparams("crawlid"), inputDataframeOfURLS.select('DOMAIN))
      None
    } else {
      val seedfile = "/dbfs/FileStore/seeds/" + availableparams("crawlid") + "seeds.txt"
      val seeds: List[String] = inputDataframeOfURLS.select('DOMAIN).map(r => r.getString(0)).collect.toList
      u.writeFile("/dbfs/FileStore/seeds/" + availableparams("crawlid") + "seeds.txt", seeds)
      val args = webCrawler.getInjectArgs(availableparams("crawlid"), inputDataframeOfURLS.select('DOMAIN), seedfile, configtofile = true)
      Some(args)
    }
  }

  /**
   * Run a crawl
   * @param url the url you want to crawl
   * @param headers the headers you want to pass
   * @param injectonly whether to inject the urls only or run a full crawl
   * @return
   */
  def runCrawl(url: String, headers: Seq[(String, String)],  injectonly : Boolean = false) : String = {

    val availableparams = getParams
    val tokenlist = getTokens(availableparams)
    var safeTokenList  = List[String]()
    tokenlist.foreach(x => {safeTokenList = URLEncoder.encode(x, "UTF-8") :: safeTokenList})
    checkParams(availableparams)

    val injectargs = initCrawl(url, availableparams, safeTokenList.toArray)
    if(!injectonly) {
      executeCrawl(availableparams, headers, "Crawls", injectargs)
    } else{
      "Inject Only Job"
    }

  }

  /**
   * Run a crawl
   * @param url the url you want to crawl
   * @param headers the headers you want to pass
   * @param injectonly whether to inject the urls only or run a full crawl
   * @return
   */
  def runJCrawl(url: String, headers: Seq[(String, String)],  injectonly : Boolean = false) : String = {

    val availableparams = getParams
    val tokenlist = getJCodeTokens(availableparams)
    var safeTokenList  = List[String]()
    tokenlist.foreach(x => {safeTokenList = URLEncoder.encode(x, "UTF-8") :: safeTokenList})
    checkParams(availableparams)

    val injectargs = initCrawl(url, availableparams, safeTokenList.toArray)
    if(!injectonly) {
      executeCrawl(availableparams, headers, "Jcode", injectargs)
    } else{
      "Inject Only Job"
    }

  }

  /**
   * Execute a crawl
   * @param availableparams the parameters available in this crawl
   * @param headers the headers you want to inject
   * @return
   */
  private def executeCrawl(availableparams: Map[String, String], headers: Seq[(String, String)], crawldir: String, injectargs: Option[Seq[String]]): String = {
    log.info("Running Crawl")
    if(availableparams("executiontype") == "Development") {
      runDevelopmentCrawl(availableparams, headers)
    } else {
      //run spark submit
      log.info("Launching in production mode")

      val m = new ManagementCore(spark)
      val c = m.getCrawl(agentName + "_" + dbutils.widgets.get("formularyid"))
      if (c.isEmpty) {
        log.info("Failed, no config found")
        "Failed, crawl not found in the database"
      } else {
        runProductionCrawl(availableparams, headers, c, injectargs, crawldir)
      }
    }
    }

  /**
   * Run a Development crawl
   * @param availableparams the parameters available in this crawl
   * @param headers the headers you want to inject
   * @return
   */
  private def runDevelopmentCrawl(availableparams: Map[String, String], headers: Seq[(String, String)]): String ={
    val webCrawler = new WebCrawler("local", installLocalSparkler = true, sparklerPath = sparklerpath)
    webCrawler.fetcherReplace.put("enable","plugin")
    webCrawler.setFetcherHeaders(headers)

    log.info("Launching in development mode")
    val i = webCrawler.crawl(availableparams("crawlid"))
    i.toString
  }

  /**
   * Set the production options in the webcrawler object
   *
   * @param webCrawler      the webcrawler
   * @param availableparams the parameters available
   * @param c               the crawl config object
   * @return
   */
  def setProdOptions(webCrawler: WebCrawler, availableparams: Map[String, String], c: Option[Crawl]): Int = {
    webCrawler.setSparkMaster("")
    webCrawler.fetcherReplace.put("enable", "plugin")
    val tokencount = getTokens(availableparams).length
    var clustersize = c.get.config("clustersize").toInt
    log.info("Found tokens: " + tokencount)
    if (tokencount < c.get.config("clustersize").toInt) {
      clustersize = getTokens(availableparams).length
    }
    log.info("Setting clustersize: " + clustersize)
    if ((tokencount / 7) < clustersize) {
      webCrawler.setRepartitionCount(clustersize)
    } else {
      val partitions = tokencount / 7
      if (partitions < clustersize) {
        webCrawler.setRepartitionCount(clustersize)
      } else {
        webCrawler.setRepartitionCount(partitions)
      }
    }
    clustersize
  }



  /**
   * Run a production crawl
   * @param availableparams the parameters available in this crawl
   * @param headers the headers you want to inject
   * @param c the config option
   * @return
   */
  def runProductionCrawl(availableparams: Map[String, String], headers: Seq[(String, String)], c: Option[Crawl],
                                 injectargs: Option[Seq[String]], crawldir: String): String ={
    val webCrawler = new WebCrawler("local", false, sparklerPath = sparklerpath)
    log.info("Configuring post crawl transform")
    webCrawler.setFetcherHeaders(headers)
    val e = generateTransformTemplate(availableparams, c, crawldir)
    val args = webCrawler.getCrawlArgs(availableparams("crawlid"), "production", Some(e))

    log.info("Calling submit job")

    val crawlsize = setProdOptions(webCrawler, availableparams, c)

    val tasks: Map[String, List[String]] = if (injectargs.isDefined) {
      Map("inject" -> injectargs.get.toList, "crawl" -> args.toList)
    } else {
      Map("crawl" -> args.toList)
    }

    val clustersize: Map[String, Int] = if (injectargs.isDefined) {
      Map("inject" -> 1, "crawl" -> crawlsize)
    } else {
      Map("crawl" -> crawlsize)
    }

    val u = new Utils

    val runid = JobAPI.generateTasksPost(availableparams("crawlid"), tasks, getPluginDir, clustersize, getSparklerVersion,
      c.get.config("clustertype"), c.get.config("scalaversion"), c.get.config("cpus"),
      c.get.config("drivermemory"), c.get.config("executormemory"), transformTemplate = e)
    val rid = u.extractRunID(runid)
    n.logCrawlStart(availableparams("crawlid"), c.get.config, c.get.config("clustertype"), c.get.config("clustersize").toInt, 0, rid)
    n.logAdditionalCrawlEvent(availableparams("crawlid"), "JOB RUN ID", runid)
    runid
  }
}
