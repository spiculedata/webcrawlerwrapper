// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package crawl

import com.databricks.dbutils_v1.DBUtilsHolder.dbutils
import com.kytheralabs.management.logger.LogHelper
import io.delta.exceptions.{ConcurrentAppendException, MetadataChangedException}

import java.net.MalformedURLException
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.{ContentType, StringEntity}
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.util.EntityUtils
import org.apache.spark.sql.DataFrame
import org.jsoup.Jsoup

import scala.collection.JavaConverters._
import java.net.{CookieHandler, CookieManager, HttpCookie, URL, URLConnection}
import scala.sys.process.Process

/**
 * Various Helper functions
 */
object Helper extends LogHelper {

  /**
   * Clean a dataframe
   * @param inputDataFrame the dataframe
   * @return
   */
  def cleanDataFrame(inputDataFrame: DataFrame): DataFrame = {
    var returnDF = inputDataFrame
    inputDataFrame.columns.foreach(columnName => {
      if (columnName.contains(".")) {
        returnDF = returnDF.drop(columnName)
      }
    })
    returnDF
  }

  /**
   * Get OAuth Token
   * @param urlstring The url to get the token from
   * @param json The JSON required in that call
   * @return
   */
  def getOAuthToken(urlstring: String, json: String): String = {
    val requestEntity = new StringEntity(
      json,
      ContentType.APPLICATION_JSON)
    val httpclient = HttpClientBuilder.create().build()
    val postMethod = new HttpPost(urlstring)
    postMethod.setEntity(requestEntity)
    postMethod.setHeader("Content-Type", "application/json")
    postMethod.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0")
    postMethod.setHeader("Host", "www.drxwebservices.com")
    postMethod.setHeader("Accept-Language", "en-GB,en;q=0.5")
    postMethod.setHeader("Origin", "https://rxtools.aetnamedicare.com")
    postMethod.setHeader("Referer", "https://rxtools.aetnamedicare.com/")


    val rawResponse = httpclient.execute(postMethod)

    val entity = rawResponse.getEntity
    val responseString = EntityUtils.toString(entity, "UTF-8")

    responseString
  }

  /**
   * Get Cookies from a URL
   * @param urlstring the url to fetch cookies from
   * @return
   */
  def getCookies(urlstring: String): List[HttpCookie] = {
    val cookieManager: CookieManager = new CookieManager()
    CookieHandler.setDefault(cookieManager)
    val url: URL = new URL(urlstring)
    val connection: URLConnection = url.openConnection()
    connection.getContent
    val cookies: List[HttpCookie] = cookieManager.getCookieStore.getCookies.asScala.toList
    for (cookie <- cookies) {
      log.debug("cookie:")
      log.debug(cookie.getDomain)
      log.debug(cookie)
    }
    cookies
  }

  /**
   * Define the base url for the crawl configs.
   * @return the base url.
   */
  def getBaseUrl(subdir: String = "Crawls"): String = {
    if(subdir.startsWith("/Shared/DevOpsCI")){
      subdir
    } else if(dbutils.notebook.getContext.notebookPath.get.startsWith("/Shared")){
      "/Shared/" + subdir
    } else{
      "/Repos/" + dbutils.notebook.getContext.tags("user") + "/Crawls/" + subdir
    }
  }
  
  /**
   * Define the base url for the jcode crawl configs.
   * @return the base url.
   */
  def getBaseUrlJcode: String = {
    if(dbutils.notebook.getContext.notebookPath.get.startsWith("/Shared")){
      "/Shared/Jcode"
    } else{
      "/Repos/" + dbutils.notebook.getContext.tags("user") + "/Crawls/Jcode"
    }
  }

  def safeAppend(dataframe: DataFrame, warehouseTable: String) : Unit = {
    var written = false
    while (!written){
      try {
        dataframe.write.format("delta").option("mergeSchema", "true").mode("append").saveAsTable(warehouseTable)
        written = true
      } catch {
        case _: ConcurrentAppendException =>
          log.info("Concurrent Append Exception Retrying")
          safeAppend(dataframe, warehouseTable)
        case _: MetadataChangedException =>
          log.info("Metadata Changed Exception Retrying")
          safeAppend(dataframe, warehouseTable)
        case _: MalformedURLException =>
          log.info("Malformed URL Exception Retrying")
          safeAppend(dataframe, warehouseTable)
      }
    }
  }
}
