// Copyright (C) 2021 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package crawl

import com.google.gson.JsonParser
import com.kytheralabs.management.logger.LogHelper
import io.delta.tables.DeltaTable
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.{HttpGet, HttpPost}
import org.apache.http.entity.{ContentType, StringEntity}
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.http.util.EntityUtils

import java.nio.charset.StandardCharsets
import java.net.URLEncoder

/**
 * Persistence Helpers
 */
class Persistence extends LogHelper {
  val httpClient: HttpClient = HttpClientBuilder.create().build()
  /**
   * Fetch the results from Solr and store them in a Spark table.
   * @param crawlId the crawl id
   * @param pTableName the tablename
   * @return
   */
  def persistResults(crawlId: String, pTableName: String, warehousename: String, spark: SparkSession, crawlSolrDbUrl: String, filterquery: String = ""): Int = {
    var tableName = warehousename + ".webcrawl_" + crawlId
    if (pTableName != null && pTableName.trim.nonEmpty) {
      tableName = pTableName
    }

    var fq = if(filterquery != null && filterquery != ""){
      "&fq=" + filterquery
    } else{
      ""
    }

    var t : DataFrame = null
    try{
      t = spark.table(tableName)
    } catch{
      case _: Throwable => log.info("Table doesn't yet exist - creating.")
    }
    if(t !=null && (fq == null  || fq == "")){
      print("calculating fq")
      val latest = spark.sql("select last_updated_at from " + tableName + " order by last_updated_at desc limit 1").first()
      val s = latest.get(0).toString
      fq = "&fq=" + URLEncoder.encode("last_updated_at:[" + s + " TO NOW]")
    }

    val get = new HttpGet(crawlSolrDbUrl + "/select?q=crawl_id:" + crawlId + "&rows=0" + fq)
    val response = httpClient.execute(get)
    val entity = response.getEntity
    val totalCountJson = EntityUtils.toString(entity, StandardCharsets.UTF_8)
    val totalCountObj = new JsonParser().parse(totalCountJson).getAsJsonObject
    val totalCount = totalCountObj.get("response").getAsJsonObject.get("numFound").getAsLong
    var recordsRetrieved = 0
    while (recordsRetrieved < totalCount) {
      recordsRetrieved = upsertRecords(crawlId, recordsRetrieved, spark, t, tableName, fq, crawlSolrDbUrl)
    }
    recordsRetrieved
  }

  def normalize(columns: Seq[String]): Seq[String] = {
    columns.map { c =>
      org.apache.commons.lang3.StringUtils.stripAccents(c.replaceAll("[ ,;{}()\n\t=]+", "_"))
    }
  }
  def upsertRecords(crawlId: String, recordsRetrieved: Int, spark: SparkSession, t: DataFrame, tableName: String, fq: String, crawlSolrDbUrl: String): Int ={
    log.info("URL to query: " + "/select?q=crawl_id:" + crawlId + "&rows=100&sort=indexed_at%20asc&start=" + recordsRetrieved + fq)

    val get = new HttpGet(crawlSolrDbUrl + "/select?q=crawl_id:" + crawlId + "&rows=100&sort=indexed_at%20asc&start=" + recordsRetrieved + fq)
    val response = httpClient.execute(get)
    val entity = response.getEntity
    val totalCountJson = EntityUtils.toString(entity, StandardCharsets.UTF_8)
    val result = new JsonParser().parse(totalCountJson).getAsJsonObject
    val inputJson = result.get("response").getAsJsonObject.get("docs")
    import spark.implicits._
    val batch = spark.createDataset(inputJson.toString :: Nil)
    val j = spark.read.json(batch)
    var dfDocs = Helper.cleanDataFrame(j)
    dfDocs = dfDocs.toDF(normalize(dfDocs.columns):_*)

    if (t != null) {

      if(dfDocs.count > 0) {
        spark.conf.set("spark.databricks.delta.schema.autoMerge.enabled", "true")
        print("UPSERTING " + dfDocs.count() + " RECORDS")
        DeltaTable.forName(spark, tableName).as("datatable").merge(
          dfDocs.as("updates"),
          "datatable.id = updates.id")
          .whenMatched
          .updateAll()
          .whenNotMatched
          .insertAll()
          .execute()
      }
    }else{
      if(dfDocs.count > 0) {
        dfDocs.write.format("delta").mode("append").option("mergeSchema", "true").saveAsTable(tableName)
      }
    }
    log.info("recordsRetrieved=" + recordsRetrieved + " : dfDocs.count=" + dfDocs.count.toInt)
    recordsRetrieved + dfDocs.count.toInt
  }

  def purgeCrawlFromSolr(crawlid: String, crawlSolrDbUrl: String= "http://ec2-35-174-200-133.compute-1.amazonaws.com:8983/solr/crawldb"): Unit ={

    val request: HttpPost = new HttpPost(crawlSolrDbUrl + "/update?commit=true")

    if(crawlid != "" && crawlid != "*"){
      val payload: String = "<delete><query>crawl_id:" + crawlid + "</query></delete>"
      val entity: StringEntity = new StringEntity(payload, ContentType.APPLICATION_XML)
      request.setEntity(entity)
      val response: HttpResponse = httpClient.execute(request)
      log.info(response)
    }
  }
}
