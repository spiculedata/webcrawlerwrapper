// Copyright (C) 2011-2012 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.

package com.kytheralabs
package document

import java.sql.Timestamp

case class MetricsRow(CrawlId: String, AgentName: String, StartDateTime: Timestamp,CompletionDateTime: Timestamp, Duration: String,
                      TotalDocumentsFound: Long, DocumentsFound: Long, PreviousCrawlDocumentsFound: Long,
                      NewDocumentsFound: Long, ChangePercentage: Float)
